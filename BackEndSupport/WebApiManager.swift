//
//  WebApiManager.swift
//  GoToRides
//
//  Created by AnandhKumar on 02/11/17.
//  Copyright © 2017 GoToRides. All rights reserved.
//

import UIKit

class WebApiManager: NSObject {
    
    /********************************************************************************/
    static let Instance : WebApiManager = WebApiManager()
    private var webServicesMap:[String: String]? = nil
    /********************************************************************************/
    
    
    private override init(){
        super.init()
        self.loadWebServicesConfig()
    }
    private func loadWebServicesConfig(){
        if let path = Bundle.main.path(forResource: "APIs", ofType: "plist") {
            
            self.webServicesMap = NSDictionary(contentsOfFile: path) as? [String:String]
            self.extendWithBaseService()
        }
    }
    private func extendWithBaseService(){
        if let baseURL = self.webServicesMap![Constants.BaseUrl]{
            for(key, value) in self.webServicesMap!{
                if key != Constants.BaseUrl{
                    self.webServicesMap![key] = baseURL + value
                }
            }
        }
    }
    
}
