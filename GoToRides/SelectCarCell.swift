//
//  SelectCarCell.swift
//  GoToRides
//
//  Created by AnandhKumar on 30/10/17.
//  Copyright © 2017 GoToRides. All rights reserved.
//

import UIKit

class SelectCarCell: UITableViewCell {

    @IBOutlet weak var BookCarButton: UIButton!
    @IBOutlet weak var carImage: UIImageView!
    @IBOutlet weak var carName: UILabel!
    @IBOutlet weak var carModel: UILabel!
    @IBOutlet weak var carAvailableStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

   
}
