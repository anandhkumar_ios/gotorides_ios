//
//  CabEnquiryController.swift
//  GoToRides
//
//  Created by AnandhKumar on 27/10/17.
//  Copyright © 2017 GoToRides. All rights reserved.
//

import UIKit

class CabEnquiryController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let firstViewController = self.storyboard?.instantiateViewController(withIdentifier: "LocalCabBookingController") as! LocalCabBookingController
        addControllerAsChild(controller: firstViewController)
        WithInCityButton.setTitleColor(UIColor.gray, for: .normal)
        OutStationButton.setTitleColor(UIColor.white, for: .normal)

    }

    @IBOutlet weak var CabAvailabilityView: UIView!
    @IBOutlet weak var OutStationButton: UIButton!
    @IBOutlet weak var WithInCityButton: UIButton!
    
    @IBAction func WithInCityButtonAction(_ sender: Any) {
       
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "OutStationCabController") as! OutStationCabController
        removeChildControllers(controller: secondViewController)
        
        let firstViewController = self.storyboard?.instantiateViewController(withIdentifier: "LocalCabBookingController") as! LocalCabBookingController
        addControllerAsChild(controller: firstViewController)
        OutStationButton.setTitleColor(UIColor.white, for: .normal)
        WithInCityButton.setTitleColor(UIColor.gray, for: .normal)

    }
    
    @IBAction func OutStationButtonAction(_ sender: Any) {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "LocalCabController") as! LocalCabController
        removeChildControllers(controller: secondViewController)
        
        let firstViewController = self.storyboard?.instantiateViewController(withIdentifier: "OutStationCabController") as! OutStationCabController
        addControllerAsChild(controller: firstViewController)
        WithInCityButton.setTitleColor(UIColor.white, for: .normal)
        OutStationButton.setTitleColor(UIColor.gray, for: .normal)

    }
    
    func addControllerAsChild(controller:UIViewController)
    {
        self.addChildViewController(controller)
        controller.view.frame = self.CabAvailabilityView.bounds
        self.CabAvailabilityView.addSubview(controller.view)
        controller.didMove(toParentViewController: self)
    }
    
    func removeChildControllers(controller:UIViewController)
    {
        controller.didMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
        controller.removeFromParentViewController()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
