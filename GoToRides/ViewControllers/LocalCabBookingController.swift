//
//  LocalCabBookingController.swift
//  GoToRides
//
//  Created by AnandhKumar on 09/11/17.
//  Copyright © 2017 GoToRides. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class LocalCabBookingController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    var locationManager:CLLocationManager!
    var mapView:MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        determineCurrentLocation()
        createMapView()
        
    }
    
     func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create and Add MapView to our main view
    }
    
     func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        determineCurrentLocation()
    }
    
    let cabBookingSeparatorView:UIView = {
        let sepView = UIView()
        sepView.backgroundColor = UIColor.white
        sepView.translatesAutoresizingMaskIntoConstraints = false
        
        
        return sepView
    }()
    
    let rideNowBookingView:UIView = {
        let sepView = UIView()
        sepView.backgroundColor = UIColor.gray
        sepView.translatesAutoresizingMaskIntoConstraints = false
        
        
        
        return sepView
    }()
    let rideLaterBookingView:UIView = {
        let sepView = UIView()
        sepView.backgroundColor = UIColor.gray
        sepView.translatesAutoresizingMaskIntoConstraints = false
        
        
        return sepView
    }()
    let localCabButton:UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Ride Now", for: .normal)
        button.titleLabel?.font = UIFont(name : "", size: 18)
        button.backgroundColor = UIColor.green
        button.addTarget(self, action: #selector(rideNowInputSetUp), for: .touchUpInside)
        
        
        return button
    }()
    
    let localRentalButton:UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Ride Later", for: .normal)
        button.titleLabel?.font = UIFont(name : "", size: 18)
        button.backgroundColor = UIColor.green
        button.addTarget(self, action: #selector(rideLaterInputSetUp), for: .touchUpInside)
        
        return button
    }()
    func createMapView()
    {
        mapView = MKMapView()
        
        let leftMargin:CGFloat = 10
        let topMargin:CGFloat = 60
        let mapWidth:CGFloat = view.frame.size.width-20
        let mapHeight:CGFloat = 300
        
        mapView.frame = CGRect(x: leftMargin, y: topMargin, width: mapWidth, height: mapHeight)
        mapView.mapType = MKMapType.standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        
        // Or, if needed, we can position map in the center of the view
        mapView.center = view.center
       // setupBookingView()
        //view.addSubview()
       // cabBookingSeparatorView.addSubview()
        view.addSubview(mapView)


    }
    
    func determineCurrentLocation()
    {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse {
            locationManager.requestWhenInUseAuthorization()
        }else{
            locationManager.startUpdatingLocation()
        }
    }
    
    private func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        switch status {
        case .authorizedWhenInUse:
            manager.startUpdatingLocation()
            break
        case .authorizedAlways:
            manager.startUpdatingLocation()
            break
        case .denied:
            //handle denied
            break
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
            break
        default:
            break
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        //manager.stopUpdatingLocation()
        
        let center = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        mapView.setRegion(region, animated: true)
        
        // Drop a pin at user's Current Location
        let myAnnotation: MKPointAnnotation = MKPointAnnotation()
        myAnnotation.coordinate = CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude);
        myAnnotation.title = "Current location"
        mapView.addAnnotation(myAnnotation)
    }
    
    private func locationManager(manager: CLLocationManager, didFailWithError error: NSError)
    {
        print("Error \(error)")
    }


   
    func setupBookingView(){
        
        view.addSubview(cabBookingSeparatorView)
        
        // x,y,w,h
        cabBookingSeparatorView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        cabBookingSeparatorView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        cabBookingSeparatorView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        cabBookingSeparatorView.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        cabBookingSeparatorView.addSubview(localCabButton)
        
        // x,y,w,h
        localCabButton.leftAnchor.constraint(equalTo: cabBookingSeparatorView.leftAnchor).isActive = true
        localCabButton.topAnchor.constraint(equalTo: cabBookingSeparatorView.topAnchor).isActive = true
        localCabButton.widthAnchor.constraint(equalTo: cabBookingSeparatorView.widthAnchor, multiplier: 1 / 2).isActive = true
        localCabButton.heightAnchor.constraint(equalToConstant: 60).isActive = true

    
        cabBookingSeparatorView.addSubview(localRentalButton)
        // x,y,w,h
        localRentalButton.leftAnchor.constraint(equalTo: localCabButton.rightAnchor).isActive = true
        localRentalButton.topAnchor.constraint(equalTo: cabBookingSeparatorView.topAnchor).isActive = true
        localRentalButton.widthAnchor.constraint(equalTo: cabBookingSeparatorView.widthAnchor, multiplier: 1 / 2).isActive = true
        localRentalButton.heightAnchor.constraint(equalToConstant: 60).isActive = true

        view.addSubview(rideNowBookingView)
        
        
        view.addSubview(rideLaterBookingView)


    }
    
    @objc func rideNowInputSetUp(){
        print("ridenow")
        
    }
    
    @objc func rideLaterInputSetUp(){
        print("ridelater")
    }
    
  
    
    
}
