//
//  LoginController.swift
//  GoToRides
//
//  Created by AnandhKumar on 30/10/17.
//  Copyright © 2017 GoToRides. All rights reserved.
//

import UIKit

class LoginController: UIViewController {

    var showOrHideBool = 0
    @IBOutlet weak var emailOrPhoneTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var showOrHideButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func loginClicked(_ sender: Any) {
    }
    
    @IBAction func signUpClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "signUpSegue", sender: nil)

    }
    @IBAction func forgotpasswordClicked(_ sender: Any) {
    }
    @IBAction func showorHideAction(_ sender: Any) {
        if showOrHideBool != 0{
            showOrHideButton.setImage(UIImage(named: "show_hide_password-07-512"), for: .normal)
            passwordTextField.isSecureTextEntry = true
            showOrHideBool = 0
        }else{
            passwordTextField.isSecureTextEntry = false
            showOrHideButton.setImage(UIImage(named: "show_hide_password-10-256"), for: .normal)
            showOrHideBool = 1
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
