//
//  OutStationCabController.swift
//  GoToRides
//
//  Created by AnandhKumar on 27/10/17.
//  Copyright © 2017 GoToRides. All rights reserved.
//

import UIKit

class OutStationCabController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelAction(_ sender: Any) {
    }
    
    @IBAction func requestCarAction(_ sender: Any) {
        self.performSegue(withIdentifier: "OutstationToLogin", sender: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
