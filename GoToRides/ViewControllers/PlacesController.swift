//
//  PlacesController.swift
//  GoToRides
//
//  Created by AnandhKumar on 06/12/17.
//  Copyright © 2017 GoToRides. All rights reserved.
//

import UIKit

class PlacesController: UIViewController {

   
    @IBOutlet weak var destinationTextField: UITextField!
    @IBOutlet weak var sourceTextField: UITextField!
    
    @IBOutlet weak var mapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonAction(_ sender: UIButton)
    {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func proceedButtonAction(_ sender: UIButton)
    {
        let licenseViewController = self.storyboard?.instantiateViewController(withIdentifier: "LicenseUploadViewController") as! LicenseUploadViewController
        self.navigationController?.pushViewController(licenseViewController, animated: true)
        
    }
}


