//
//  SelfDriveAvailabilityController.swift
//  GoToRides
//
//  Created by AnandhKumar on 27/10/17.
//  Copyright © 2017 GoToRides. All rights reserved.
//

import UIKit

class SelfDriveAvailabilityController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var selectACarTableView: UITableView!
    
    var carImageArray = [UIImage(named: "carimage"),UIImage(named: "carimage"),UIImage(named: "carimage"),UIImage(named: "carimage")]
    var carNameArray = ["Audi Q7","BMW","Swift","Safari"]
    var carModelArray = ["Audi Q7 , Diesel 2017","BMW, autoGear,2016","Swift Diesel 2015","Safari, 2014, diesel"]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }

    @IBAction func cancelAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return carNameArray.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 165
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! SelectCarCell

        cell.carName.text = carNameArray[indexPath.row]
        cell.carModel.text = carModelArray[indexPath.row]
        cell.carImage.image = carImageArray[indexPath.row]

        
        return cell
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
