//
//  localCabRentalRequestCntroller.swift
//  GoToRides
//
//  Created by AnandhKumar on 06/12/17.
//  Copyright © 2017 GoToRides. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation


class localCabRentalRequestCntroller: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var destinationTextField: UITextField!
    @IBOutlet weak var sourceTextField: UITextField!
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    
    @IBOutlet weak var localRental: UIButton!
    @IBOutlet weak var OutStation: UIButton!
    @IBOutlet weak var bottomView: UIView!
    var locationManager = CLLocationManager()
    var city = String()
    var state = String()
    var country = String()
    var zipCode = String()
    var countryCode = String()
    var locname = ""
    
    @IBOutlet weak var dummyView: UIView!
    var currenttextField : UITextField!
    var latitude = Double()
    var longitude = Double()
    var startLocationName = ""
    var endLocationName = ""
    var startLocationCoordinate = CLLocation()
    var endLocationCoordinate = CLLocation()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        currenttextField = sourceTextField
        sourceTextField.delegate = self
        destinationTextField.delegate = self
        
        
        mapView.addSubview(locationView)
        
        
        //Check this where should be placed
        self.mapView.delegate = self
        self.mapView.isMyLocationEnabled = true
        self.mapView.settings.myLocationButton = true
        self.mapView.settings.compassButton = true
        self.mapView.settings.allowScrollGesturesDuringRotateOrZoom = true
        
        for gesture in mapView.gestureRecognizers! {
            mapView.removeGestureRecognizer(gesture)
        }
        findMyLocation()
        setUpBottomView()
        
    }
    
    func setUpBottomView(){
        
        localRental.frame = CGRect(x: 0, y: 0, width: bottomView.frame.width / 2, height: bottomView.frame.height)
        OutStation.frame = CGRect(x: view.frame.width / 2, y: 0, width: bottomView.frame.width / 2, height: bottomView.frame.height)
        bottomView.addSubview(localRental)
        bottomView.addSubview(OutStation)

    }
    
    func findMyLocation(){
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: {(placemarks, error)->Void in
            
            if (error != nil) {
                print("Reverse geocoder failed with error" + error!.localizedDescription)
                return
            }
            
            if placemarks!.count > 0 {
                let pm = placemarks![0]
                self.displayLocationInfo(pm)
            } else {
                print("Problem with the data received from geocoder")
            }
        })
    }
    
    func displayLocationInfo(_ placemark: CLPlacemark?) {
        if let containsPlacemark = placemark {
            //stop updating location to save battery life
            locationManager.stopUpdatingLocation()
            
            print(containsPlacemark)
            let locality = (containsPlacemark.locality != nil) ? containsPlacemark.locality : ""
            let postalCode = (containsPlacemark.postalCode != nil) ? containsPlacemark.postalCode : ""
            let administrativeArea = (containsPlacemark.administrativeArea != nil) ? containsPlacemark.administrativeArea : ""
            let countryName = (containsPlacemark.country != nil) ? containsPlacemark.country : ""
            print(" Postal Code \(String(describing: postalCode))")
            print(" administrativeArea \(String(describing: administrativeArea))")
            print(" country \(String(describing: country))")
            print(" locality\(String(describing: locality))")
            print(" sublocality \(String(describing: containsPlacemark.subLocality))")
            
            sourceTextField.text = "\(containsPlacemark.name!)" + "," + locality! + "," + countryName!
            self.country = countryName!
        }
        
    }
    
    
    //        @IBAction func takeRideButtonAction(_ sender: UIButton)
    //        {
    //            let plcaesViewController = self.storyboard?.instantiateViewController(withIdentifier: "PlacesViewController") as! PlacesViewController
    //            self.navigationController?.pushViewController(plcaesViewController, animated: true)
    //        }
    //
    //
    //        @IBAction func offerRideButtonAction(_ sender: ButtonExtender)
    //        {
    //            let plcaesViewController = self.storyboard?.instantiateViewController(withIdentifier: "PlacesViewController") as! PlacesViewController
    //            self.navigationController?.pushViewController(plcaesViewController, animated: true)
    //        }
    func createMarker(tilteMarker:String,iconMarker:UIImage,latitude:CLLocationDegrees,longitude:CLLocationDegrees)
    {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(latitude, longitude)
        marker.title = tilteMarker
        marker.map = mapView
        marker.icon = iconMarker
    }
    func drawPath(startLocation:CLLocation,endLocation:CLLocation)
    {
        let origin = "\(startLocation.coordinate.latitude),\(startLocation.coordinate.longitude)"
        
        let destination = "\(endLocation.coordinate.latitude),\(endLocation.coordinate.longitude)"
        
        
        let urlString = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving"
        
        guard let url = URL(string:urlString)else
        {
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if error != nil
            {
                print(error?.localizedDescription ?? "")
                return
            }
            
            do
            {
                let jsonDict : Dictionary = try JSONSerialization.jsonObject(with: data!, options: []) as! Dictionary<String,Any>
                print(jsonDict)
                
                if let routes = jsonDict["routes"] as? Array<Dictionary<String,Any>>
                {
                    print(routes)
                    DispatchQueue.main.async
                        {
                            
                            for route in routes
                            {
                                if let overViewPloyline = route["overview_polyline"] as? Dictionary<String,Any>
                                {
                                    if let points = overViewPloyline["points"] as? String
                                    {
                                        let path = GMSPath.init(fromEncodedPath: points)
                                        let polyline = GMSPolyline(path: path)
                                        polyline.strokeColor = UIColor.blue
                                        polyline.strokeWidth = 3.0
                                        polyline.map = self.mapView
                                        
                                    }
                                }
                            }
                    }
                }
            }
            catch
            {
                
            }
            
            }.resume()
        
    }
    func showLocation(location:CLLocationCoordinate2D?)
    {
        if location != nil
        {
            mapView.camera = GMSCameraPosition.camera(withTarget: location!, zoom: 15)
        }
    }
}

extension localCabRentalRequestCntroller : UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        currenttextField = textField
        let placesController = GMSAutocompleteViewController()
        placesController.delegate = self
        self.present(placesController, animated: true, completion: nil)
        textField.resignFirstResponder()
    }
}
extension localCabRentalRequestCntroller : GMSAutocompleteViewControllerDelegate
{
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace)
    {
        latitude = place.coordinate.latitude
        longitude = place.coordinate.longitude
        
        if currenttextField == sourceTextField
        {
            sourceTextField.text = place.formattedAddress
            startLocationName = place.name
            startLocationCoordinate = CLLocation(latitude: latitude, longitude: longitude)
            
            let locationCoordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            showLocation(location: locationCoordinate)
            
            createMarker(tilteMarker: "Start", iconMarker:#imageLiteral(resourceName: "map-pin"), latitude: latitude, longitude:longitude)
        }
        else
        {
            destinationTextField.text = place.name
            endLocationName = place.name
            endLocationCoordinate = CLLocation(latitude: latitude, longitude: longitude)
            
            let locationCoordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            showLocation(location: locationCoordinate)
            
            
            createMarker(tilteMarker: "End", iconMarker:#imageLiteral(resourceName: "Location_end"), latitude: latitude, longitude:longitude)
            
            drawPath(startLocation: startLocationCoordinate, endLocation: endLocationCoordinate)
            
            
        }
        dismiss(animated: true, completion: nil)
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error)
    {
        print(error.localizedDescription)
        dismiss(animated: true, completion: nil)
        
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController)
    {
        dismiss(animated: true, completion: nil)
    }
}


extension localCabRentalRequestCntroller : GMSMapViewDelegate
{
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        mapView.isMyLocationEnabled = true
    }
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        
        if gesture
        {
            mapView.selectedMarker = nil
        }
    }
    
}






